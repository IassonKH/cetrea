/*
  PCINT2

  Pin D0 TX Serial Programming/debug
  Pin D1 RX Serial Programming/debug
  Pin D2 Anemometer Interrupts for wind direction (green wire)
  Pin D3 Anemometer Interrupts for wind speed (yellow wire)
  Pin D4 Enable 1
  Pin D5 Enable 2
  Pin D6
  Pin D7 SDI-12 Data Pin for soil sensors(blue wire)

  PCINT0

  Pin D8
  Pin D9  TX from GPS
  Pin D10 Thermometer Data
  Pin D11 RX Iridium Satellite
  Pin D12 TX Iridium Satellite
  Pin D13 Led

  PCINT1

  Pin A0 Rain Polling
  Pin A1
  Pin A2
  Pin A3 Temperature + humidity sensor Vin
  Pin A4 SDA from RTC
  Pin A5 SCL from RTC
  Pin A6 Analog only
  Pin A7 Analog only
*/

//------------------------------------Application Notes------------------
/*
  Transistor must be used for powering up GPS, Iridium with a digital pin. GPS cannot receive while comms with Iridium are going and vice-versa.

    Digital Pins 0-7  - Port D - PCINT2
    Digital Pins 8-13 - Port B - PCINT0
    Analog Pins A0-A5 - Port C - PCINT1

  SDI and software Serial both try to assign this pins to a ISR. SDI is in pcint 2(pin 7), sw keeps everyother one
  modify .cpp by commenting out declaration as seen here depending on application:
  https://github.com/EnviroDIY/Arduino-SDI-12/issues/8
  Software Serial.cpp is in your computer at Arduino/hardware/arduino/avr/libraries/SoftwareSerial/src

  DO NOT USE ANY SOFTWARE SERIALS ON DIGITAL PINS 0-7!!!

  SDI12 library taken from here: https://github.com/EnviroDIY/Arduino-SDI-12
  Iridium library taken from here:https://github.com/mikalhart/IridiumSBD
  RTClibrary taken from here: https://github.com/adafruit/RTClib
  SimpleTimer library taken from here: https://github.com/jfturcot/SimpleTimer
*/
//------------------------------------Definitions------------------------

#define DS1307_I2C_ADDRESS 0x68


//ISRs numbers for external interrupts
#define vaneInterrupt 0
#define anemometerInterrupt 1

//Pins 4 and 5 for transistor switching, 13 is debug Led
#define vanePin         2
#define anemometerPin   3
#define IridiumControl  4
#define GPSControl      5
#define DATAPIN         7
#define Thermos         10
#define LED             13

//Number tries I will make to send Data through satellite link
#define SatelliteTries 10

//bit position of each flag
#define send_Data           1
#define thermoBounce        2
#define newGPSLine          4
#define gpsRoutineDone      8
#define RTCFail             16
#define GPSPresent          32
#define ThermoPresent       64
#define min30               128
#define noGPS_Signal        1
#define success             2
#define noAltitude          4
#define found               8
#define first               16
#define triggered           32
#define interruptAnemometer 1
#define interruptVane       2
#define cycle1              4
#define cycle2              8
#define seconds5            16
#define windGust            32
#define debugFlag           64


//-------------------------------------Macros------------------------------
//Set to 1,0 or read bit at "pos" of byte "by" respectively
#define BOOL_TRUE(by,pos)  by|=pos
#define BOOL_FALSE(by,pos) by&= ~(pos)
#define BOOL_READ(by,pos)  bool(by&pos)

//-------------------------------------Libraries---------------------------
#include "IridiumSBD.h"
#include <OneWire.h>
#include "SoftwareSerial.h"
#include "SimpleTimer.h"
#include "RTClib.h"
#include "avr/power.h"
#include <AM2320.h>

//----------------------------------Modify time intervals-----------------

#define measurementsNumber 3 //times I will take readings before sending data

//----------------------------------Constants & Global variables----------

bool firstRun = true;
byte flags1 = 0;
byte flags2 = 0;
byte flags3 = 0;


unsigned long dayChangeCounter = 0; // Counter to change day
short takeReading = 0; //value in seconds between each data acquisition

SimpleTimer timerSeconds; // timer for seconds declared with SimpleTimer Library

byte readTries = 0; //Count how many times I've tried to read data
byte second5Count = 0;  //Seconds counter to register last wind speed and angle
byte second60Count = 0; //minute counter to check for wind gust and update arrays
byte pos10Array = 0; //Position in 10 wind average I'm currently at

//variables changed within interrupts for the vane. anemometerDifference
//returned to 0 when no interrupts have happened in the last 5 seconds
volatile unsigned long previousAnemometer = 0;
volatile unsigned long newAnemometer = 0;
volatile unsigned long anemometerDifference = 0;

//vane global time data
volatile unsigned long newVane = 0;
volatile unsigned long vaneDifference = 0;
//averages for wind data
volatile float strenghtGust = 0;
volatile float avg3Hours = 0;
volatile float movAvgWindDirection = 0;

unsigned long referenceTime = 0; //will reset on year 2038
//days counter
long daysCount = 0;

float vanePos   = 0.0;   //last angle° from reference vane has

//arrays for data
float hygroCelsius[measurementsNumber] = {0, 0, 0};
float hygroHumidity[measurementsNumber] = {0, 0, 0};

float movingAvg[12];  //avg of wind Speeds of last 60 seconds, each value represents last wind speed of last 5 seconds
float movingMax[10];  //maximum wind Speeds of the last 10 minutes, each position is the max wind speed of a whole minute
float movingMin[10];  //minimum wind Speeds of the last 10 minutes, each position is the min wind speed of a whole minute

float movingAvg2min = 0;  //avg of moving avg of the last 2 min
float avgLastMinute = 0;  //to save last minute avg
float avgThisMinute = 0;  //to save current last minute avg

float in = 0; // inches of first minute converted to millimeters by the rain gauge
float mm = 0; // inches of second minute to give average of 2 minutes
float rainAvg[6] = { -1, -1, -1, -1, -1, -1};
bool rainState;

//receive buffer for serial comms
char rc;

SoftwareSerial nss(11, 12);  //Iridium object created
IridiumSBD isbd(nss, 13); //to sleep iridium, not really used
RTC_DS1307 rtc;  //instance rtc object
AM2320 th;

//---------------------------------------External Interrupts-------------------------------------------
/*Anemometer Interrupt Routine
  Everytime an interrupt is fired in anemometerPin it will update the new time in case there isn't an overflow
  then it will check if we got interrupts in correct order and update values right away
*/
void anemometer() {

  //save time value and detach interrupt to avoid multiple firing of routine
  detachInterrupt(digitalPinToInterrupt(anemometerPin));
  newAnemometer = millis();

  //incase of overflow skip reading
  if (newAnemometer - previousAnemometer > newAnemometer + previousAnemometer) {
    previousAnemometer = newAnemometer;
    attachInterrupt(digitalPinToInterrupt(anemometerPin), anemometer, RISING);
    return;
  }
  //if we have 2 anemometers interrupts between one vane interrupt
  if ((newVane >= previousAnemometer) && (newVane <= newAnemometer)) {
    BOOL_TRUE(flags3, cycle1); //prepare cycle to calculate needed data
    anemometerDifference = newAnemometer - previousAnemometer; //save values before updating data
    vaneDifference = (anemometerDifference) - (newAnemometer - newVane);
  }
  previousAnemometer = newAnemometer; //update data for next interrupt and re-arm it
  attachInterrupt(digitalPinToInterrupt(anemometerPin), anemometer, RISING);
}


//Vane Interrupt Routine, save time data, un-arm and re-arm interrupt to avoid multiple firings
void vane() {
  detachInterrupt(digitalPinToInterrupt(vanePin));
  newVane = millis();
  attachInterrupt(digitalPinToInterrupt(vanePin), vane, RISING);
}

//-----------------------------------------------------MAIN------------------------------------------
/*
  Method runs once at start up, initializes everything
*/
void setup() {
  //--------------------------Serial Debug-----------------------------------
  //sets serial comms
  setSerial();
  //initialize sensors and environment
  initialize();

  //--------------------------RTC Module-------------------------------------
  setRTC();

  //--------------------------First Run--------------------------------------
  //Gets reference time
  readDS1307time();

  //enable external interrupts on respective Pins
  attachInterrupt(digitalPinToInterrupt(anemometerPin), anemometer, RISING);
  attachInterrupt(digitalPinToInterrupt(vanePin), vane, RISING);
  readData();
}
/*
  Equivalent of main with a while(1){}
  updates time, then checks if enough time passed to update wind info and then polls rain pin
*/
void loop() {

  timerSeconds.run();
  //----------------Data Read----------------------------------------
  windRoutine();
  rainCheck();
  //readData();
  //----------------Data Send----------------------------------------
  //sendData();

}

//-------------------------Serial Comms-----------------------------------------------
/*
  Waits for serial comm to start, this depends on the module on the arduino and not on the actual connection with another device
*/
void setSerial() {
  Serial.begin(115200);
  while (!Serial) {
    //wait for port to open
  }
}

/*
  Sets pins, flags, and routines
*/
void initialize() {

  //Serial.println(F("Boot"));
  //define pins as outputs and inputs
  pinMode(LED, OUTPUT);  //debug on board led

  pinMode(IridiumControl, OUTPUT); //transistor to Satellite
  digitalWrite(IridiumControl, LOW); //turn off Iridium

  pinMode(GPSControl, OUTPUT); //transistor to GPS
  digitalWrite(GPSControl, LOW);    //turn off GPS

  pinMode(A3, OUTPUT); //pin designed to turn on/off humidity + temperature sensor
  digitalWrite(A3, LOW); //turn off humidity + temperature sensor

  pinMode(anemometerPin, INPUT); //interrupt pins turned on for wind vane
  pinMode(vanePin, INPUT);

  pinMode(A0, INPUT);  //pin polled for rain sensor
  //turn Iridium,GPS hygrometer off

  rainState = digitalRead(A0); //get initial rain meter state

  //library is in ms, will enter "secondsCount" every second approx
  timerSeconds.setInterval(1000, secondsCount);

  //flags for system
  BOOL_TRUE(flags1, send_Data);
  BOOL_FALSE(flags1, thermoBounce);
  BOOL_TRUE(flags1, newGPSLine);
  BOOL_FALSE(flags1, gpsRoutineDone);
  BOOL_FALSE(flags1, RTCFail);
  BOOL_TRUE(flags1, GPSPresent);
  BOOL_TRUE(flags1, ThermoPresent);
  BOOL_TRUE(flags1, min30);
  BOOL_FALSE(flags2, noGPS_Signal);
  BOOL_FALSE(flags2, success);
  BOOL_FALSE(flags2, noAltitude);
  BOOL_FALSE(flags3, interruptAnemometer);
  BOOL_FALSE(flags3, interruptVane);
  BOOL_FALSE(flags3, cycle1);
  BOOL_TRUE(flags3, debugFlag);

  interrupts();
  Serial.println(F("R"));
}
//-------------------------------Data Read---------------------------
//reads thermometer, humidity, RTC
void readData() {

  byte thermoCounter = 0;

  //power up internal modules of arduino
  powerUpRoutine();

  //get time if possible from RTC
  readDS1307time();

  //gets humidity AND temperature
  humidityRead();

  //updates Rain array with present values
  rainUpdate();

  readTries++;

  //Serial.print(F("Count:"));
  //Serial.print(readTries);
  //Serial.print(F(" of "));

  //checks if it is time to send data. readTries counts up to measurementsNumber
  //also checks if it isn't the first run since startup
  //Serial.println(measurementsNumber);
  if ( (readTries >= measurementsNumber) || firstRun ) {
    sendData();
  } else {
    //Power down internal modules to save power
    lowPowerRoutine();
  }

}

//------------------------------------Data Send--------------------------
/*
  Send data through satellite uplink
*/
void sendData() {

  //Serial.println(F("Sending data"));
  //get data from GPS
  //gpsRoutine();

  //try to send Data. If no satellite is connecte|zd data will be sent via serial comms
  satelliteRoutine();

  //enter low power consumption mode
  lowPowerRoutine();
  firstRun = false;
  //reset wind Data found
  strenghtGust = 0;
  avg3Hours = 0;
  movAvgWindDirection = 0;
  //reset amount of times I've read sucessfully
  readTries = 0;

}

//-----------------------------------------------Extra Timer------------------------------

//Timer for time period, executed very second
void secondsCount() {
  //Serial.println(seconds);
  //flash led
  digitalWrite(LED, !digitalRead(LED));
  //seconds counts days, takeReading data acquisition period, 5 count and 60 count are for windVane

  takeReading++;
  second5Count++;
  second60Count++;

  if (second5Count >= 5) {
    //flag to register last read speed and vanePos
    BOOL_TRUE(flags3, seconds5);
    second5Count = 0;
  }
  //reading every 60 minutes, updates day if 24 hours passed
  if (takeReading >= 3600) {
    //Serial.println(F("3 hours passed"));
    dayChangeCounter++;
    takeReading = 0;

    if (dayChangeCounter >= 24) {
      daysCount++;
      if (BOOL_READ(flags1, RTCFail)) {
        referenceTime = referenceTime + 86400;
      }
      dayChangeCounter = 0;
    }

    readData();
  }
  /*If RTC died, count extra days every 8 times we read data. 8*3 hour period=24 hours
  */

}


//-----------------------------------------------Wind Vane Routines--------------------------------------------
/*
  If interrupts came in the correct order, calculate values
  then, if 5 seconds have passed update arrays
  then, if 60 seconds also have passed update averages and wind gust data
*/
void windRoutine() {

  float windSpeed;
  //If during interrupt I've read data in the correct order
  if (BOOL_READ(flags3, cycle1)) {
    noInterrupts(); //turn off interrupts

    BOOL_FALSE(flags3, cycle1);

    windSpeed = speed(anemometerDifference);
    vanePos = angleDegrees(vaneDifference, anemometerDifference);
    //error checking-noise conditions
    if ( (windSpeed != 0.0) && (vanePos != 360.0) && (vanePos > 0.0) ) {
      //Serial.print(F("mph "));
      ////Serial.println(windSpeed, 2);
      //Serial.print(F("a "));
      //Serial.println(vanePos, 2);
      //if data is correct detach interrupts to avoid noise until 5 seconds have passed
      detachInterrupt(digitalPinToInterrupt(vanePin));
      detachInterrupt(digitalPinToInterrupt(anemometerPin));
    }//if it was noise turn interrupts on again
    interrupts();
  }
  //if 5 seconds have passed register in array last wind data read
  if (BOOL_READ(flags3, seconds5)) {
    BOOL_FALSE(flags3, seconds5);
    windSpeed = speed(anemometerDifference);
    shiftArray(windSpeed);
    //reset data

    if (vanePos >= 0 || vanePos < 360) {
      //get moving Wind Average
      movAvgWindDirection = movAvgWindDirection + (((vanePos - movAvgWindDirection)) / 180);
    } else {
      vanePos = 0;
      movAvgWindDirection = movAvgWindDirection + (((vanePos - movAvgWindDirection)) / 180);

    }
    //Serial.print(F("°: "));
    //Serial.println(vanePos, 2);
    //Serial.print(F("W dir:"));
    //Serial.println(movAvgWindDirection);

    anemometerDifference = 0;
    vaneDifference = 0;
    attachInterrupt(digitalPinToInterrupt(anemometerPin), anemometer, RISING);
    attachInterrupt(digitalPinToInterrupt(vanePin), vane, RISING);
  }

  if (second60Count >= 60) {
    //reset counter, get Average and save for this minute
    second60Count = 0;


    avgThisMinute = getAverage(12, movingAvg);
    //Serial.print(F("avg:"));
    //Serial.println(avgThisMinute);
    //save Maximum value from array in moving Max and increment counter
    movingMax[pos10Array] = findMaximum(12, movingAvg);
    //Serial.print(F("Max:"));
    //Serial.println(movingMax[pos10Array]);
    movingMin[pos10Array] = findMinimum(12, movingAvg);
    //Serial.print(F("Min:"));
    //Serial.println(movingMin[pos10Array]);
    pos10Array++;
    //reset after last position was reached
    if (pos10Array > 9) {
      pos10Array = 0;
    }
    //get average from last 2 averages, then update values

    avg3Hours += avgThisMinute / 180;
    movingAvg2min = (avgThisMinute + avgLastMinute) / 2;
    avgLastMinute = avgThisMinute;

    //Serial.print(F("WindSpeed:"));
    //Serial.println(avg3Hours);

    checkGust();

  }
}

/*if there's already a gust check if it's still going
  if not, check if it started*/

void checkGust() {
  //Serial.println(F("Checking Gust"));
  //if biggest maximum of last 10 min - last average of this minute is >= 3 knots 3.45 mph
  if (BOOL_READ(flags3, windGust)) {
    if (strenghtGust < speed(anemometerDifference)) {
      strenghtGust = speed(anemometerDifference);
    }
    //Serial.print(F("still exists with: "));
    //Serial.print(findMaximum(10, movingMax));
    //Serial.print(F("-"));
    //Serial.println(avgThisMinute);
    if ( (findMaximum(10, movingMax) - avgThisMinute) >= 3.45) {
      //Serial.println(F("It stopped now"));
      BOOL_FALSE(flags3, windGust);
    }
  } else {

    //Serial.print(F("Avg is:"));
    //Serial.println(avgThisMinute);
    //check if minute average is >= than 9 knots 10.35mph
    if ( avgThisMinute >= 10.35) {

      //Serial.println(F("it exists"));
      //check if maximum-minimum of last ten minutes is >= than 10 knots 11.5078mph
      if ( (findMaximum(10, movingMax) - findMinimum(10, movingMin)) >= 11.5 ) {
        //check if difference between max speed and average of last 10 minutes is >= 5 knots 5.75mph
        if ((findMaximum(10, movingMax) - avgThisMinute) >= 5.75) {
          BOOL_TRUE(flags3, windGust);
          strenghtGust = speed(anemometerDifference);
        }
      }
    }
  }
}



//given ms between closures, returns speed in mph
float speed(long closureRate) {
  float rps = 1000.0 / (float)closureRate;

  if (0.010 < rps && rps < 3.23) {
    return -0.11 * (rps * rps) + 2.93 * rps - 0.14;
  }
  else if (3.23 <= rps && rps < 54.362) {
    return 0.005 * (rps * rps) + 2.20 * rps + 1.11;
  }
  else if (54.36 <= rps && rps < 66.33) {
    return 0.11 * (rps * rps) - 9.57 * rps + 329.87;
  }
  else {
    return 0.0;
  }
}

/*
 gives angle of wind vane in °
*/
float angleDegrees(long vaneDifference, long anemometerDifference) {
  float angle = (((float)vaneDifference / (float)anemometerDifference) * (float)360.0);
  while (angle > 360) {
    angle = angle - 360;
  }
  angle = 360 - angle;
  return angle;
}

//--------------------------------------Hygrometer-----------------------------------------

/*
 turns hygrometer on, sends reading request, then registers each piece of info
 */
void humidityRead() {

  digitalWrite(A3, HIGH); //turn on humidity + temperature sensor
  delay(5000);

  String val = F(""); //string for end result
  byte tries = 0;//try to get data 10 times
  float humidity;

//th.Read() returns number, if it's 0 deivce is ready
  while (tries < 10) {
    switch (th.Read()) {
      case 2:
        tries++;
        break;
      case 1:
        //Serial.println(F("Sensor off"));
        tries++;
        break;
      case 0:
    //if it isn't the first Run save data in respective place of array
    //if it is save in pos 0 of array
        if (!firstRun) {

          hygroHumidity[readTries] = th.Humidity;

          if (hygroHumidity[readTries] == 0 ) {
            tries++;
            break;
          }

          hygroCelsius[readTries] = th.cTemp;

          if (hygroCelsius[readTries] == 0 ) {
            tries++;
            break;
          }

          tries = 11;
          break;

        } else {

          hygroHumidity[0] = th.Humidity;

          if (hygroHumidity[0] == 0 ) {
            tries++;
            break;
          }

          hygroCelsius[0] = th.cTemp;

          if (hygroCelsius[0] == 0 ) {
            tries++;
            break;
          }

          tries = 11;
          break;
        }
    }
  }

  //Serial.print(F("H read"));
  //Serial.println(hygroHumidity[readTries]);

  //Serial.print(F("C read"));
  //Serial.println(hygroCelsius[readTries]);
  digitalWrite(A3, LOW); //turn off humidity + temperature sensor
}

//---------------------------------------Rain Gauge----------------------------------------
/*
 Method polls pin, if state was 0 and it's now 1 ten drops of rain have fallen
 if it was 1 and it's now 0 update state
 */
void rainCheck() {
  if ( (rainState == 0) && (digitalRead(A0))) {
    //Serial.println(digitalRead(A0));
    in = in + 0.01;
    mm = in * 25.4;
    rainState = 1;
    Serial.print(F("mm: "));
    Serial.println(mm);
  } else if ( (rainState == 1) && ( !digitalRead(A0)) ) {
    rainState = 0;
  }
}
/*
 resets rain data of last hour and updates array
 */
void rainUpdate() {

  shiftRainArray();
}

/*
   Shifts the 6 hour array to the right, and returns the value of the last hour to zero.
*/
void shiftRainArray() {
  float shift = rainAvg[0];
  float swap;
  for (int i = 0; i < 6; i ++) {
    swap = rainAvg[i];
    rainAvg[i] = shift;
    shift = swap;
  }

  rainAvg[0] = mm;
  in = 0;
  mm = 0;
  //Serial.println();
}


//-----------------------------------------------RTC Management--------------------------------------
/*
 Starts RTC
*/
void setRTC() {
  //RTCFail =1 in case we can't connect to it
  if (! rtc.begin()) {
    BOOL_TRUE(flags1, RTCFail);
  } else {
    BOOL_FALSE(flags1, RTCFail);
  }
}

/*
Reads RTC and updates its state if this wasn't possible
*/
void readDS1307time() {
  DateTime now = rtc.now();

  //checks if RTC is not present
  if ((now.year() == 2165 || now.month() == 165 || now.year() == 165) && (now.unixtime() != 0) ) {
    BOOL_TRUE(flags1, RTCFail);
    //Serial.println(F("RTC dead"));
  } else {
    BOOL_FALSE(flags1, RTCFail);
    //Serial.println(now.unixtime());
  }

  //update todays reference time only if RTC's working and readtries has reset
  if ( (readTries == 0 && !(BOOL_READ(flags1, RTCFail))) || firstRun ) {
    referenceTime = now.unixtime();
  }

}


//---------------------------------------------------------------Arithmetic Methods---------------------------

//this methods recevie an array and a size, then they find the max or minx value in given array inside the first SIZE spaces
float findMaximum(int SIZE, float avgArray[]) {
  float highest = avgArray[0];
  for (int i = 0; i < SIZE; i++) {
    if (highest < avgArray[i]) {
      highest = avgArray[i];
    }
  }
  return highest;
}

float findMinimum(int SIZE, float avgArray[]) {
  float lowest = avgArray[0];
  for (int i = 0; i < SIZE; i++) {
    if (lowest > avgArray[i]) {
      lowest = avgArray[i];
    }
  }
  return lowest;
}

//this method recevies an array and a size, then it calculates the average value in given array inside the first SIZE spaces
float getAverage(int SIZE, float movingAvg[]) {

  float average = 0;
  for (byte i = 0; i < SIZE; i++) {
    average = average + movingAvg[i];
  }
  average = average / SIZE;
  return average;
}

//shift wind Array to the left, then stores lastest reading
void shiftArray(float windSpeed) {
  memmove(&movingAvg[0], &movingAvg[1], sizeof(movingAvg) - sizeof(*movingAvg));
  movingAvg[11] = windSpeed;
}

//----------------------------------------------------------Iridium Management------------------------------

//blink lead while sending data with iridium
boolean ISBDCallback() {
  digitalWrite(LED_BUILTIN, (millis() / 1000) % 2 == 1 ? HIGH : LOW);
  return true;
}

/*
 Starts Iridium turns it on, waits a few seconds, builds frame and sends it
 */
void satelliteRoutine() {

  detachInterrupt(digitalPinToInterrupt(anemometerPin));
  detachInterrupt(digitalPinToInterrupt(vanePin));
  byte tries = 0; //time out fatal error variable for satellite comm
  //Turn Iridium on. Wait for start up, being serial comm, wait a bit and start reading.
  digitalWrite(IridiumControl, HIGH);
  //150 sec for start up time, 120 sec minimum

  delay(60000);
  delay(60000);
  delay(30000);

  nss.begin(19200);

  isbd.attachConsole(Serial);
  isbd.setPowerProfile(1);
  isbd.adjustSendReceiveTimeout(50);
  isbd.begin();
  frameRoutine();

  //reset variables for Iridium, turn off module
  BOOL_FALSE(flags2, success);

  isbd.sleep();
  nss.end();
  digitalWrite(IridiumControl, LOW);

  //Serial.flush();

  attachInterrupt(digitalPinToInterrupt(anemometerPin), anemometer, RISING);
  attachInterrupt(digitalPinToInterrupt(vanePin), vane, RISING);
}

//Build frame depending on what Data was available/is requiered
void frameRoutine() {

  int signalQuality = -1; //default value for iridium modem minimum signal quality
  //possible to prealocate char array with malloc and keep track of current pos with counter, more efficient?

  //reserve() method makes an "array" of the string and instead of causing memory fragmentation, makes it so
  //the string is manipulated inside it. once the method ends space is freed
  String stringFrame;
  String temporal;


  //this pre-allocates a 270 char buffer in memory to work with.
  //no memory fragmentation will happen as String will be rewritten only inside the 270 char space
  if (!(stringFrame.reserve(270))) {
    //Serial.println(F("No space"));
    return;
  }

  stringFrame = F("7,GX8K2F3T,");
  byte tries = 0;

  //Serial.print(F("GPS:"));
  temporal = String_GPS();
  stringFrame += temporal;
  //Serial.println(temporal);

  //Serial.print(F("RTC:"));
  temporal = String_RTC();
  stringFrame += temporal;
  //Serial.println(temporal);

  //Serial.print(F("Rain:"));
  temporal = String_Rain();
  stringFrame += temporal;
  //Serial.println(temporal);

  //Serial.print(F("SolarRad:"));
  temporal = String_SolarRad();
  stringFrame += temporal;
  //Serial.println(temporal);

  //Serial.print(F("Hygro:"));
  temporal = String_Higrometer();
  stringFrame += temporal;
  //Serial.println(temporal);

  //Serial.print(F("Bar:"));
  temporal = String_Barometer();
  stringFrame += temporal;
  //Serial.println(temporal);

  //Serial.print(F("Wind Vane:"));
  temporal = String_WindVane();
  stringFrame += temporal;
  //Serial.println(temporal);

  stringFrame += String(daysCount);

  temporal = String_CRC(stringFrame);
  stringFrame = String(F("&")) + stringFrame + temporal;
  char *chars = const_cast<char*>(stringFrame.c_str());

  Serial.println(F(""));
  Serial.println(chars);
  Serial.flush();

  int err;
//tries to get signal quality then decides if it's a good idea to send data or try again
  while (BOOL_READ(flags2, success) == 0) {
    if (tries < SatelliteTries) {
      err = isbd.getSignalQuality(signalQuality);

      if (err != 0) {
        //Serial.print(F("Sig Quality fail: err "));
        //Serial.println(err);
        tries++;
      }

      //Serial.print(F("Quality "));
      //Serial.println(signalQuality);
      if (signalQuality < 1) {
        tries++;
        err = 11;
      } else {
        err = isbd.sendSBDText(chars);
      }
      if (err != 0) {
        //Serial.print(F("sendText err "));
        //Serial.println(err);
        tries++;
      } else {
        BOOL_TRUE(flags2, success);
      }
    } else {
      BOOL_TRUE(flags2, success);
      //Serial.println(F("Error with SDB module"));
    }
    delay(6000);
  }

  //Serial.println(F("Routine Done"));
}

/*
 Gets data if RTC is present
 */
String String_RTC() {

  if (referenceTime == 0) {
    BOOL_TRUE(flags1, RTCFail);
  }

  String RTC_String = F("");
  //timestamp
  if (!(BOOL_READ(flags1, RTCFail))) {
    //Serial.println(F("RTC present"));
    RTC_String += String(referenceTime, DEC);
    RTC_String += F(",");
  } else {
    RTC_String += F(",");
  }
  return RTC_String;
}


/*
 Gets data if GPS is present. Data will be hard coded server side this time
 */
String String_GPS() {

  String GPS_String = F("");

  GPS_String = F(",,,");

  //timestamp
  return GPS_String;
}



/*
 Gets data if piranometer is present
 */
String String_SolarRad() {
  String solar = F(",,,,");
  return solar;
}


/*
 Gets data if wind vane is present
 */
String String_WindVane() {

  String meditions = "";

  //1mph=0.447 m/s
  if (avg3Hours == 0) {
    meditions += F("0,");
  } else {
    meditions += String((avg3Hours * 0.447), 3);
    meditions += F(",");
  }

  if (movAvgWindDirection == 0) {
    meditions += F("0,");
  } else {
    meditions += String(movAvgWindDirection, 3);
    meditions += F(",");
  }

  if (strenghtGust == 0) {
    meditions += F("0,");
  } else {
    meditions += String((strenghtGust * 0.447), 3);
    meditions += F(",");
  }

  return meditions;
}


/*
 Gets data if pluviometer is present.
 saves value of the first three hours separately and then of then the average of the last three
 */
String String_Rain() {

  String rain = "";      //final string
  float averages = 0;  //to temporarily hold numbers
  int cont = 0;          //counts total available numbers for averages

  if (rainAvg[0] == -1 || (rainAvg[0] == 0) ) {
    rainAvg[0] = 0;
    rain += F("0,");
  } else {
    averages = rainAvg[0];
    rain += String(averages, 2);
    rain += F(",");

  }

  if (rainAvg[1] == -1 || (rainAvg[1] == 0) ) {
    rain += F("0,");
  } else {
    averages = rainAvg[1];
    rain += String(averages, 2);
    rain += F(",");
  }

  if (rainAvg[2] == -1 || (rainAvg[2] == 0) ) {
    rain += F("0,");
  } else {
    averages = rainAvg[2];
    rain += String(averages, 2);
    rain += F(",");
  }

  averages = 0;

  for (int j = 3; j < 6; j ++) {
    if (rainAvg[j] == -1) {
      rain += F("0,");
      cont = 0;
      break;
    } else {
      cont = cont + 1;
      averages = averages + rainAvg[j];
    }
  }
  if (cont != 0) {
    averages = averages / cont;

    if (averages == 0) {
      rain += F("0,");
    } else {
      rain += String(averages, 2);
      rain += F(",");
    }

  }

  return rain;
}



/*
 Gets data if Barometer is present
 */
String String_Barometer() {
  String pressure = F(",");
  return pressure;
}


/*
 Gets data if Higrometer is present
 */

String String_Higrometer() {

  String val = "";
  float temp = 0;
  if (firstRun) {

    if (hygroCelsius[0] != 0) {
      val += String(hygroCelsius[0], 2);
      val += F(",");
    } else {
      val += F("0,");
    }

    if (hygroCelsius[0] != 0) {
      val += String(hygroCelsius[0], 2);
      val += F(",");
    } else {
      val += F("0,");
    }

    if (hygroCelsius[0] != 0) {
      val += String(hygroCelsius[0], 2);
      val += F(",");
    } else {
      val += F("0,");
    }

    if (hygroHumidity[0] != 0) {
      val += String(hygroHumidity[0], 2);
      val += F(",");
    } else {
      val += F("0,");
    }

  } else {

    temp = findMinimum(3, hygroCelsius);
    //Serial.print(F("Min C:"));
    //Serial.println(temp);
    val += String(temp, 2);
    val += F(",");

    temp = getAverage(3, hygroCelsius);
    //Serial.print(F("Avg C:"));
    //Serial.println(temp);
    val += String(temp, 2);
    val += F(",");

    temp = findMaximum(3, hygroCelsius);
    //Serial.print(F("Max C:"));
    //Serial.println(temp);
    val += String(temp, 2);
    val += F(",");

    temp = getAverage(3, hygroHumidity);
    //Serial.print(F("Prom H:"));
    //Serial.println(temp);
    val += String(temp, 2);
    val += F(",");
  }
  return val;
}


/*
 Calculates CRC from String
 */
String String_CRC(String stringFrame) {
  String CRC_String = "";
  String crc;

  byte bytes[stringFrame.length() + 1];
  stringFrame.getBytes(bytes, stringFrame.length() + 1);
  byte crc8 = CRC8(bytes, stringFrame.length());


  if (String(crc8, HEX).length() == 1) {
    crc = F("0");
    crc += String(crc8, HEX);
  } else {
    crc = String(crc8, HEX);
  }

  CRC_String = F("*");
  CRC_String +=   crc;

  return CRC_String;
}

byte CRC8(const byte * data, byte len) {
  byte crc = 0x00;
  while (len--) {
    crc ^= *data++;
    for (byte i = 0; i < 8; i++) {
      if (crc & 0x80) crc = (crc << 1) ^ 0x07;
      else crc <<= 1;
    }
  }
  return crc;
}

//---------------------------------------------------------Low Power------------------------------

/*
 Disables indiviual not used modules
 */
void lowPowerRoutine() {
  //Disable adc
  ADCSRA = 0;
  //turn off unused modules until time to read comes
  power_adc_disable(); // ADC converter
  power_spi_disable(); // SPI
  //power_usart0_disable();// Serial (USART)
  //power_timer0_disable();// Timer 0 not turned off to keep millis() running
  power_timer1_disable();// Timer 1
  power_timer2_disable();// Timer 2
  power_twi_disable(); // TWI (I2C)
}


/*
 Powers up modules
 */
void powerUpRoutine() {
  //ADC, never used, never turned on
  power_spi_enable(); // SPI
  //power_usart0_enable(); // Serial (USART)
  //power_timer0_enable(); // Timer 0 never turned off, cause millis() resets
  power_timer1_enable(); // Timer 1
  power_timer2_enable(); // Timer 2
  power_twi_enable(); // TWI (I2C)
  delay(5000);
}
